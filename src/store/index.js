import Vue from 'vue'
import Vuex from 'vuex'

import payments from './modules/payment/payment'
import authentication from './modules/authentication/authentication'
import dialog from './modules/dialog/dialog'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    payments,
    authentication,
    dialog
  }
})
