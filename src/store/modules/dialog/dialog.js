
const state = {
  iframeDialog: false
}

const getters = {
  GetIframeDialog (state) {
    return state.iframeDialog
  }
}

const mutations = {
  SetIframeDialog (state, dialog) {
    console.log('change dialog bolean')
    state.iframeDialog = dialog
  }
}

const actions = {
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
