import axios from 'axios'
import Cookies from 'js-cookie'
import router from '@/router'

const state = {
  data: {
    username: '',
    password: ''
  },
  token: Cookies.get('session') || ''
}

const getters = {
  GetUsername (state) {
    return state.data.username
  },
  GetPassword (state) {
    return state.data.password
  },
  GetToken (state) {
    console.log(state.token)
    return state.token
  }
}

const mutations = {
  SetUsername (state, username) {
    state.data.username = username
  },
  SetPassword (state, password) {
    state.data.password = password
  },
  SetToken (state, token) {
    state.token = token
    Cookies.set('session', token, { secure: true, sameSite: 'strict', expires: 7 })
    // router.push('/billpayment')
  },
  ClearToken (state) {
    state.token = ''
    Cookies.remove('session')
    router.push('/testcomponents')
  }
}

const actions = {
  async signIn ({ state, commit }) {
    try {
      await axios.post('http://localhost:3000/users/login', state.data).then((response) => {
        commit('SetToken', response.data.token)
        commit('dialog/SetIframeDialog', true, { root: true })
      })
      await router.push('/billpayment')
    } catch (error) {
      console.log(error.message)
    }
  },
  async logOut ({ commit }) {
    await commit('ClearToken')
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
