import axios from 'axios'

const state = {
  payments: [],
  search: '',
  paymentspages: 'title',
  date_start: null,
  date_end: null
}

const getters = {
  GetPayments (state) {
    return state.payments
  },
  GetPaymentsPages (state) {
    return state.paymentsPages
  },
  GetStartDate (state) {
    return state.date_start
  },
  GetEndDate (state) {
    return state.date_end
  }
}

const mutations = {
  setPayments (state, payments) {
    state.payments = payments
  },
  UpdateSearchKeyValue (state, search) {
    state.search = search
  },
  UpdatePaymentPage (state, title) {
    state.paymentspages = title
  },
  SetStartDateTime (state, startDate) {
    state.date_start = startDate
  },
  SetEndDateTime (state, endDate) {
    state.date_end = endDate
  },
  SetClearStateValues (state) {
    state.date_start = null
    state.date_end = null
  }
}

const actions = {
  async ReadPayment ({ commit, rootState }) {
    try {
      const token = rootState.authentication.token
      console.log(token)
      axios.get('http://localhost:3000/payment/', {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).then((response) => {
        commit('setPayments', response.data.data)
      })
    } catch (error) {
      console.error('Error fetching payment:', error)
    }
  },
  async QueryPaymentByDate ({ commit, rootState }) {
    try {
      const token = rootState.authentication.token
      if (state.date_start && state.date_end) {
        axios.get(`http://localhost:3000/payment/filter?start_date=${state.date_start}&end_date=${state.date_end}`, {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }).then((response) => {
          console.log(response.data)
          commit('setPayments', response.data)
        })
      }
    } catch (error) {
      console.log('Error fetching payment:', error)
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
