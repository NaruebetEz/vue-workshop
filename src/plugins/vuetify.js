import Vue from 'vue'
import Vuetify from 'vuetify/lib/framework'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    themes: {
      light: {
        aquamarine: '#4fa894',
        red: '#F44336'
      }
    },
    typography: {
      fontFamily: 'Noto Sans Thai, sans-serif'
    }
  }
})
