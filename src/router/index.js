import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '',
    name: 'toolbar',
    component: () => import('../views/Toolbar.vue'),
    children: [
      {
        path: '/me',
        name: 'me',
        component: () => import('../views/Me.vue')
      },
      {
        path: '/responsivetest',
        name: 'responsivetest',
        component: () => import('../views/ResponsiveTest.vue')
      }
    ]
  },
  {
    path: '',
    name: 'paymenttoolbar',
    component: () => import('../views/Billpayment.vue'),
    children: [
      {
        path: '/table',
        name: 'table',
        component: () => import('../views/Billpaymenttable.vue')
      }
    ]
  },
  {
    path: '/me',
    name: 'me',
    component: () => import('../views/Me.vue')
  },
  {
    path: '/testcomponents',
    name: 'testcomponents',
    component: () => import('../views/Testcomponent.vue')
  },
  {
    path: '/simple',
    name: 'simple',
    component: () => import('../views/Simple.vue')
  },
  {
    path: '/grade',
    name: 'grade',
    component: () => import('../views/Grade.vue')
  },
  {
    path: '/apicon',
    name: 'apicon',
    component: () => import('../views/Apicon.vue')
  },
  {
    path: '/responsivetest',
    name: 'responsivetest',
    component: () => import('../views/ResponsiveTest.vue')
  },
  {
    path: '/order',
    name: 'order',
    component: () => import('../views/Order.vue')
  },
  {
    path: '/billpayment',
    name: 'billpayment',
    component: () => import('../views/Billpayment.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/bill',
    name: 'bill',
    component: () => import('../views/Billist.vue')
  },
  {
    path: '/charts',
    name: 'charts',
    component: () => import('../components/PieCharts.vue')
  },
  {
    path: '/ecommerce',
    name: 'ecommerce',
    component: () => import('../views/Ecommerce/Navbar.vue'),
    children: [
      {
        path: 'home',
        name: 'home',
        component: () => import('../views/Ecommerce/Home/Home.vue')
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters['authentication/GetToken']) {
      next({
        path: '/testcomponents',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
